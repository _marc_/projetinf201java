package application;


import java.util.ArrayList;

import application.model.*;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
//import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;

public class Controleur extends Application {
	//attributs
	//pas des vraies constantes, m'enfin bon
	
	private String BD = "bdinf201";
	private String PORT = "3306";
	private String USERNAME = "root";
	private String M2P = "";
	
	//attributs permettant le stockage des th�saurus
	private Thesaurus[] thesaurus = {null,null};
	private boolean the=false;
	
	/*
	Variables de javaFX peremttant  un lien entre le controleur  et la  vue
	*/
	@FXML
	private TextField labelBd;
	@FXML
	private TextField labelPort;
	@FXML
	private TextField labelUsername;
	@FXML
	private PasswordField labelM2p;
	@FXML
    private TextField req1Idpat;
	@FXML
	private ChoiceBox<String> req3Sexe;
	@FXML
	private Button req2Button;
	@FXML
	private TextField req2idPat;
	@FXML
	private DatePicker req2Deb;
	@FXML
	private DatePicker req2Fin;
	@FXML
	private Text ecranCtrl; 
	@FXML
	private AnchorPane ancre;

	
	
	@Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("VuePI201.fxml"));
            
            
            
            primaryStage.setTitle("Projet Java 201 - Consultation Donn�es PMSI - Marc Fouqu�");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
                        
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
	
	private void fillThesaurus(String BD,String PORT, String U, String M) {
		if(!the) {
			System.out.println("donn�es th�saurus");
			try {
				System.out.println("CCAM");
				thesaurus[0] = new Thesaurus("tab_ccam", "Classification Commune des Actes M�dicaux",BD,PORT,U,M);
				System.out.println("CIM10");
				thesaurus[1] = new Thesaurus("tab_cim10","Classification Internationale Medicale",BD,PORT,U,M);
			}catch(Exception e) {
				System.err.println(e+" - "+e.getMessage());
			}
			System.out.println("___");
			this.the = true;
		}
		
	}
	private void fillThesaurus() {
		fillThesaurus(BD,PORT,USERNAME,M2P);
	}
	
	private void afficheur(String s) {
		System.out.println("Affichage r�sultat");
		//System.out.println(ecranCtrl.getText());
		if(s.trim().length()==0)s="R�sultat de la requ�te nul,\n Verifier les parametres de la requ�te et ceux de la connexion � la base de donn�es";
		ecranCtrl.setText(s);
		ancre.setPrefHeight(ecranCtrl.getText().length()*0.7);				
		
	}
	private void afficheur(ArrayList a) {
		String s = "";
		for(int i = 0; i<a.size(); i++) {
			s += a.get(i)+"\n";
		}
		afficheur(s);
		//resul2.forEach((n)->System.out.println(n));
	}
	
	@FXML
	private void initialize() {
		System.out.println("init Controleur");
		ObservableList<String> obl = FXCollections.observableArrayList("Homme","Femme");
        req3Sexe.getItems().addAll(obl);
        System.out.println("req3 ChoiceBox remplie");
        
		System.out.println("___");
	}
	@FXML
	private void controlParamBD() {
		System.out.println("Maj Params BD");
		System.out.println(this.labelBd.getText());
		this.BD=this.labelBd.getText();
		this.PORT=this.labelPort.getText();
		this.USERNAME=this.labelUsername.getText();
		this.M2P=this.labelM2p.getText();
		System.out.println("___");
		the = false;
		fillThesaurus();
	}
	
	@FXML
	private void requete1() {
		System.out.println("requete 1");
		this.fillThesaurus();
		int[] detailRetour = {0,3,1,0};//recuperation d'actes et de diagnostics imbriqu�s dans leur hospitalisation affili�e					ArrayList resul = new ArrayList();
		String requete = "SELECT * FROM `tab_acte`,tab_hospitalisation,tab_diagnostic where tab_acte.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION and tab_diagnostic.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION and tab_hospitalisation.ID_PATIENT="+this.req1Idpat.getText();
		System.out.println(requete);
		System.out.println("requete lanc�e");
		ArrayList resul = UtilitaireRequete.requetePmsi(requete,detailRetour, thesaurus,BD,PORT,USERNAME,M2P);
		System.out.println(resul.size());
		System.out.println("requete effectuee");
		//resul.forEach((n)->System.out.println(n));
		afficheur(resul);
	}
	@FXML
	private void requete2() {
		System.out.println("requete 2");
		System.out.println(req2Deb.getValue());
		this.fillThesaurus();
		int[] detailRetour2 = {0,0,0,0}; //trivial, retour num�rique
		ArrayList resul2 = new ArrayList();
		String requete2 = "select count(ID_AKT) from tab_acte,tab_hospitalisation where tab_hospitalisation.ID_PATIENT in (Select ID_PATIENT from tab_patient where ID_PATIENT="+req2idPat.getText()+") and tab_acte.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION and tab_acte.DATE_ATK>='"+req2Deb.getValue()+"' and tab_acte.DATE_ATK<='"+req2Fin.getValue()+"'  group by tab_hospitalisation.ID_PATIENT";
		System.out.println("requete lanc�e");
		resul2 = UtilitaireRequete.requetePmsi(requete2,detailRetour2, thesaurus,BD,PORT,USERNAME,M2P);
		System.out.println("requete effectuee");
		//resul2.forEach((n)->System.out.println(n));
		afficheur(resul2);
	}
	@FXML
	private void requete3() {
		System.out.println("requete 3");
		this.fillThesaurus();
		int sexe = (String.valueOf((this.req3Sexe.getValue())).equals("Homme")?1:2);
		int[] detailRetour3 = {1,0,0,0};// retour d'une liste de classification
		ArrayList resul3 = new ArrayList();
		String requete3 = "Select CODE_CIM10 as code_classi from tab_diagnostic,tab_hospitalisation where tab_hospitalisation.ID_PATIENT in (Select ID_PATIENT from tab_patient where sexe="+sexe+") and tab_diagnostic.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION group by CODE_CIM10 Order by count(ID_DISGNOCTIC) DESC LIMIT 5;";
		System.out.println("requete lanc�e");
		resul3 = UtilitaireRequete.requetePmsi(requete3,detailRetour3, thesaurus,BD,PORT,USERNAME,M2P);
		System.out.println("requete effectuee");
		//resul3.forEach((n)->System.out.println(n));
		
		
		afficheur(resul3);
	}
	    
	public static void main(String[] args) {
		
		System.out.println("AppPMSI");
		launch(args);
	}


	
}
