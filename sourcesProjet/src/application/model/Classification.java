package application.model;
/**
 * Permet de repr�senter les �l�ments d'une classification (ex: ccam ou cim10) chaque �l�ment comportant un cde et un libell�
 * @see Thesaurus
 * @author Marc
 *
 */
public class Classification {
	/**
	 * id repr�sentant l'identifiant de l'�l�ment class�
	 * label repr�sentant le nom de l'�l�ment class�
	 */
	private String id;
	private String label;
	
	/**
	 * Constructeur
	 * @param i l'identifiant de l'�l�ment
	 * @param l le nom de l'�l�ment
	 */
	public Classification (String i, String l) {
		this.id=i;
		this.label=l;
	}
	
	/**
	 * Permet de r�cup�rer l'identifiant de l'�l�ment
	 * @return this.id
	 */
	public String getId() {
		return id;
	}
	/**
	 * Permet de r�cup�rer le nom de l'�l�ment
	 * @return this.label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * Permet de modifier l'identifiant de l'�l�ment
	 * @param i
	 */
	public void setId(String i) {
		this.id = i;
	}
	/**
	 * Permet de modifier le nom de l'�l�ment
	 * @param l
	 */
	public void setLabel(String l) {
		this.label = l;
	}
	
	@Override
	public String toString() {
		return "_"+id+" "+label+"_";
	}
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Classification c = (Classification) o;
		if (!id.equals(c.getId())) return false;
		return true;
	}
	
}
