package application.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;
/**
 * Classe permettant de regrouper des m�thodes (fonctions) permettant d'effectuer des taches de liaison � la base de donn�es et de formatage desdits donn�es
 * @author Marc
 *
 */
public class UtilitaireRequete {
	/**
	 * Methode permettant d'initialiser et d'ouvrir la connection � la base de donn�es et stipulant l'algorithme d'utilisation des m�thodes d�crites ci-apr�s
	 * @param req chaine caract�res repr�sentant la requete sql 
	 * @param retour tableau de 4 entiers, permettant de d�crire le type d�sir� dans la liste de retour;\n\t la position 0, exclusive, est binaire et est stipule un retour Classification;\n\t la position 1 est quaternaire, concerne les retours Operation,0 l'abscence, 1 que Actes, 2 que Diagnostik et 3 les deux;\n\t la position 2 est binaire et stipule un retour Hospitalisation;\n\t la position 3, est binaire et stipule un retour Patient. Les trois derni�res positions �tant cumulables par imbrications de leurs types. Un tableau trivial donne un type de retour primitif.
	 * @param CC10 tableau de 2 stockant les th�saurus
	 * @return reto une liste des entit�s r�cup�r�es.
	 * @see Classification
	 * @see Acte
	 * @see Diagnostik
	 * @see Hospitalisation
	 * @see Patient
	 */
	public static ArrayList requetePmsi(String req,int[] retour, Thesaurus[] CC10, String BD, String PORT, String U, String M) {
		//initialisation du resultat
		ArrayList reto = new ArrayList();
		//initialisation du stockage d'hospitalisation
		HashMap<Integer,Hospitalisation> hh = new HashMap<Integer,Hospitalisation>();

		
		try{  
			//initialisation de laliaison � la base de donn�es
			Properties p = new Properties();
			p.put("user", U);p.put("password", M);
			p.put("serverTimezone", "Europe/Paris");
			//Class.forName("com.mysql.jdbc.Driver");  
			Connection con=DriverManager.getConnection(  "jdbc:mysql://localhost:"+PORT+"/"+BD,p); 	
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery(req); 
			
			//arbre de d�cision on part du plus haut Personne pour descendre dans l'imbrication jusqu'aux Classifcation
			//seuls d�tails, 
			//l'utilisation d'un hashmap pour les hospitalisation utile pour l'affectation des actes et diagnostics
			//et parcours double des r�sultats dans le cas d'un r�sultat voulu de patient + hospitalisation, pour lister les hospitalisations(et leurs affecter le cas d'�ch�ant des operations) puis les affecter aux patients
			if(retour[3]==1) {
				if(retour[2]==0) {
					while(rs.next()) {
						reto.add(getReqPatient(rs));
					}
				}
				else {
					while(rs.next()) formatHospitalisation(rs, hh, CC10, retour[1]);
					while(rs.previous()) {
						Patient patient = getReqPatient(rs);
						if(reto.contains(patient)) {
							int ind = reto.indexOf(patient);
							patient=(Patient)reto.get(ind);
							reto.remove(ind);
						}
						patient.addSejour(hh.get(rs.getInt("tab_hospitalisation.ID_HOSPITALISATION")));
						reto.add(patient);
					}
				}
			}
			else {
				while(rs.next()) {
					if(retour[2]==0) {
						if(retour[1]==0) {
							if(retour[0]==1) {
								if(req.contains("ccam")) reto.add(getReqClassification(rs,CC10[0]));
								else reto.add(getReqClassification(rs,CC10[1]));
							}
							else {
								reto.add(rs.getString(1));
							}
						}
						else if(retour[1]==1)reto.add(getReqOperation(rs, true, CC10[0]));
						else if(retour[1]==2)reto.add(getReqOperation(rs,false,CC10[1]));
						else if(retour[1]==3){
							reto.add(getReqOperation(rs, true, CC10[0]));
							reto.add(getReqOperation(rs,false,CC10[1]));
						}
					}
					else {
						formatHospitalisation(rs, hh, CC10,retour[1]);
					}
				}
			}
			//fermture de la liaison
			con.close();  
		}catch(Exception e){ System.out.println(e);}
		//pour le cas des hospitalisations
		if(retour[2]==1 && retour[3]==0) reto = new ArrayList<Hospitalisation>(hh.values());
		if(reto.isEmpty())System.out.println("Resultat vide, changez vos parametres");
		return reto;
	}
	
	/*
	 * Toutes les fonctions qui suivent permettent de g�rer la r�cup�ration des r�sultats apr�s connexion � la base de donn�es
	 * Elles permettent aussi de clarifier le code du cheminement du programme
	 */
	
	/**
	 * Permet de r�cup�rer une Classification d'un tuple de r�sulat
	 * @param rs les r�sultats de la requ�te 
	 * @param CC10 le th�saurus affili�
	 * @return la classification donn�e
	 * @see Classification
	 */
	private static Classification getReqClassification(ResultSet rs,Thesaurus CC10) {
		Classification c=null;
		try {
			if(CC10.getNom().equals("Classification Commune des Actes M�dicaux")) c= CC10.getClassiFromCode(rs.getString("code_classi"));
			else c= CC10.getClassiFromCode(rs.getString("code_classi"));
		}catch(Exception e) {System.err.println(e.getMessage());}
		return c;
	}
	/**
	 * Peremt de r�cup�rer une Op�ration d'un tuple de r�sultat
	 * @param rs les r�sultats de la requ�te
	 * @param akt un bool�en stipulant si c'est un Acte (true) ou un diagnostic (false)
	 * @param CC10 stocke le th�saurus utile
	 * @return un Acte ou un Diagnostik cr�� � partir des r�sultats
	 */
	private static Operation getReqOperation(ResultSet rs,boolean akt,Thesaurus CC10) {
		Operation o=null;
		try {
			if(akt)o= new Acte(rs.getString("tab_acte.ID_AKT"),CC10.getClassiFromCode(rs.getString("tab_acte.ID_CCAM")),rs.getDate("tab_acte.DATE_ATK"),rs.getBoolean("tab_acte.ANESTH"));
			else o= new Diagnostik(rs.getString("tab_diagnostic.ID_DISGNOCTIC"),CC10.getClassiFromCode(rs.getString("tab_diagnostic.CODE_CIM10")),rs.getInt("tab_diagnostic.DRANG"),rs.getString("tab_diagnostic.DGTYPE").charAt(0));
		}catch(Exception e) {System.err.println(e.getMessage());}
		return o;
	}
	/**
	 * Permet de r�cup�rer une Hospitalisation d'un tuple de r�sulat
	 * @param rs Les r�sultats de la requ�te
	 * @param actes une liste d'actes � affecter � l'hospitalisation
	 * @param diagnos une liste de diagnostic �affecter � l'hospitalisation
	 * @return une Hospitalisation cr��e � partir des r�sultats
	 */
	private static Hospitalisation getReqHospitalisation(ResultSet rs,ArrayList<Acte> actes,ArrayList<Diagnostik> diagnos) {
		Hospitalisation h=null;
		try {
			h=new Hospitalisation(rs.getInt("tab_hospitalisation.ID_HOSPITALISATION"),actes,diagnos,rs.getDate("tab_hospitalisation.DATE_ENTREE"),rs.getDate("tab_hospitalisation.DATE_SORTIE"));
		}catch(Exception e) {System.err.println(e.getMessage());}
		return h;
	}
	/**
	 * Permet de r�cup�rer une Hospitalisation d'un tuple de r�sultat
	 * @param rs Les r�sultats de la requ�te
	 * @return une Hospitalisation cr��e � partir des r�sultats
	 */
	private static Hospitalisation getReqHospitalisation(ResultSet rs) {
		return getReqHospitalisation(rs, new ArrayList<Acte>(), new ArrayList<Diagnostik>());
	}
	/**
	 * Permet de r�cup�rer un Patient d'un tuple de r�sultat
	 * @param rs les r�sultats de la requ�te
	 * @param hs une liste des hospitalisations � affecter au patient
	 * @return un patient cr�� � partir des r�sultats
	 */
	private static Patient getReqPatient(ResultSet rs,ArrayList<Hospitalisation> hs) {
		Patient p=null;
		try {
			p=new Patient(rs.getString("tab_Patient.ID_PATIENT"),rs.getInt("tab_patient.SEXE"),rs.getDate("tab_patient.DATE_NAISSANCE"),rs.getString("tab_patient.PRENOM"),rs.getString("tab_patient.NOM"),hs);
		}catch(Exception e) {System.err.println(e.getMessage());}
		return p;
	}
	/**
	 * Permet de r�cup�rer un Patient d'un tuple de r�sultat
	 * @param rs les r�sultats de la requ�te
	 * @return un patient cr�� � partir des r�sultats
	 */
	private static Patient getReqPatient(ResultSet rs) {
		return getReqPatient(rs,new ArrayList<Hospitalisation>());
	}
	/**
	 * Permet de g�rer l'affectation multiple d'op�rations (diagnostic et actes) � une hospitalisation
	 * @param rs Les r�sultats de la requ�te
	 * @param hh le dictionnaire (objet, map,...) stockant les diff�rentes hospitalisations de la requ�te
	 * @param CC10 les th�saurus
	 * @param retour1 un entier nous signifiant ce que l'on prend en compte, les actes, les diagnostics, les deux ou rien
	 */
	private static void formatHospitalisation(ResultSet rs,HashMap<Integer,Hospitalisation> hh, Thesaurus[] CC10, int retour1) {
		Hospitalisation h = getReqHospitalisation(rs);
		if(hh.containsKey(h.getId())) {
			h=hh.get(h.getId());
			hh.remove(h.getId());
		}
		if(retour1!=0) {
			if(retour1!=1) {
				Diagnostik d = (Diagnostik)getReqOperation(rs,false,CC10[1]);
				if(!h.getDiagnos().contains(d))h.addOperation(d);
			}
			if(retour1!=2){
				Acte a = (Acte)getReqOperation(rs,true,CC10[0]);
				if(!h.getActes().contains(a))h.addOperation(a);
			}
		}
		hh.put(h.getId(),h);

	}
}
