package application.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
/**
 * Permet de repr�senter des th�saurus de classification (ex: ccam,cim10) ayant un nom (nom) et une liste de'�l�ment (theso) �tant des Classification
 * @see Classification
 * @author Marc
 *
 */
public class Thesaurus {
	private ArrayList<Classification> theso;
	private String nom;
	
	//constructeur
	/**
	 * Constructeur
	 * constructeur allant chercher via une base de donn�es les �l�ments du th�saurus
	 * @param tab chaine de caracterre d�crivant le nom de la table d'o� est tir� le th�saurus
	 * @param nom nom du th�saurus
	 * @param BD nom de la base de donn�es de laquelle on tire les informations
	 * @param PORT num�ro de port sur lequel MySQL �coute
	 */
	public Thesaurus(String tab, String nom,String BD,String PORT, String U, String M) {
		System.out.println(tab);
		if(tab!="tab_cim10" && tab!="tab_ccam")theso=null;
		else {
			this.nom = nom;
			this.theso = new ArrayList();
			System.out.print("Recuperation du theausrus "+nom+" :");
			try{  
				//Class.forName("com.mysql.jdbc.Driver");  
				Properties p = new Properties();
				p.put("user", U);p.put("password", M);
				//p.put("user", "root");p.put("password", "");
				p.put("serverTimezone", "Europe/Paris");
				Connection con=DriverManager.getConnection(  
						"jdbc:mysql://localhost:"+PORT+"/"+BD,p); 
				
				Statement stmt=con.createStatement();  
				ResultSet rs=stmt.executeQuery("select * from "+tab);  
				while(rs.next()) {
					Classification c = new Classification(rs.getString(1), rs.getString(2));
					//System.out.println(c);
					this.theso.add(c);
					//System.out.println(rs.getString(1)+" ____ "+ rs.getString(2));
				}
				System.out.println("Done");
				con.close();  
			}catch(Exception e){ System.out.println(e);}
		}
	}

	//getter
	/**
	 * Permet de r�cup�rer la liste des �l�ments du th�saurus
	 * @see ArrayList
	 * @see Classification
	 * @return theso une liste des �l�ments du th�saurus
	 */
	public ArrayList<Classification> getTheso() {
		return theso;
	}
	/**
	 * Permet de r�cup�rer le nom du th�saurus
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	//methode
	/**
	 * Permet de r�cup�rer un �l�ment du th�saurus � partir de son code d'identification
	 * @param id l'identifiant unique de l'�l�ment
	 * @return une Classification
	 * @see Classification
	 * @see Classification.id
	 */
	public Classification getClassiFromCode(String id) {
		try {
			Classification temp = new Classification(id,"");
			return this.theso.get(this.theso.indexOf((Object)temp));
			}
		catch(Exception e) {
			System.out.println("code inexistant");
			return new Classification("0000","Aucun code correspondant");
			}

	}
	
	public String toString() {
		String s = " "+this.nom;
		//s+="\n"+this.theso.get(56);
		//this.theso.forEach((i) -> s.concat("\n\t"+i));
		for(int i=0; i<this.theso.size();i++) {
			s+="\n\t"+this.theso.get(i);
		}
		return s;
	}
	
}
