package application.model;
/**
 * Permet de repr�senter une op�ration �ff�ctu�e lors d'un s�jour � l'hopital (ex: Acte, Diagnostic), gr�ce � un code identifiant l'op�ration et une classification affili�e
 * @see Acte
 * @see Diagnostik
 * @author Marc
 * 
 */
public class Operation {
	private String id;
	private Classification code;
	
	//constructeurs
	/**
	 * Constructeur
	 * @param i identifiant unique de l'op�ration
	 * @param c Classification de l'acte
	 * @see Classification
	 */
	public Operation(String i, Classification c) {
		this.id = i;
		this.code = c;
	}
	/**
	 * Constructeur
	 * @param i identifiant unique de l'op�ration
	 */
	public Operation(String i) {
		this(i, null);
	}
	
	//getters
	/**
	 * Permet de r�cup�rer l'identifiant
	 * @return this.id
	 * @see String
	 */
	public String getId() {
		return id;
	}
	/**
	 * Permet de r�cup�rer le code de l'op�ration
	 * @return this.code
	 * @see Classification
	 */
	public Classification getCode() {
		return code;
	}
	
	
	@Override
	/**
	 * Redefinition de la methodde equals, comparaison exclusivement par le biai de l'id
	 * @see this.id
	 */
	public boolean equals(Object obj) {
		if (this == obj)return true;
		if (obj == null || getClass() != obj.getClass())return false;
		Operation o = (Operation) obj;
		if (id == null) {
			if (o.id != null)
				return false;
		} else if (!id.equals(o.id))
			return false;
		return true;
	}
	
	public String toString() {
		return id;
	}
	
	
}
