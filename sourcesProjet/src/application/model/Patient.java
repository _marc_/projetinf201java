package application.model;

import java.util.ArrayList;
import java.util.Date;
/**
 * Permet de repr�senter un patient par un identifiant unique, son sexe, sa date de naissance, son prenom, son nom et la liste des s�jours � l'hopital qu'il a effectu�
 * @see Hospitalisation
 * @author Marc
 *
 */
public class Patient {
	private String id;
	private int sexe;
	private Date dateNaissance;
	private String prenom;
	private String nom;
	private ArrayList<Hospitalisation> sejours;
	
	//constructeurs
	/**
	 * Constructeur
	 * @param i identifiant unique du patient
	 * @param s sexe du patient (1:homme, 2:femme)
	 * @param dn date de naissance du patient
	 * @param p prenom du patient
	 * @param n nom du patient
	 * @param hs liste des hospitalisations du patient
	 */
	public Patient(String i, int s, Date dn, String p, String n,ArrayList<Hospitalisation> hs) {
		id=i;sexe=s;dateNaissance=dn;prenom=p;nom=n;sejours=hs;
	}
	/**
	 * Constructeur
	 * @param i identifiant unique du patient
	 * @param s sexe du patient (1:homme, 2:femme)
	 * @param dn date de naissance du patient
	 * @param p prenom du patient
	 * @param n nom du patient
	 */
	public Patient(String i, int s, Date dn, String p, String n) {
		this(i,s,dn,p,n,new ArrayList<Hospitalisation>());
	}

	//getters
	/**
	 * Permet de r�cup�rer l'identifiant du patient
	 * @return this.id
	 */
	public String getId() {
		return id;
	}
	/**
	 * Permet de r�cup�rer le sexe du patient
	 * @return this.sexe
	 */
	public int getSexe() {
		return sexe;
	}
	/**
	 * Permet de r�cup�rer la date de naissance du patient
	 * @return this.dateNaissance
	 */
	public Date getDateNaissance() {
		return dateNaissance;
	}
	/**
	 * Permet de r�cup�rer le pr�nom du patient
	 * @return this.prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Permet de r�cup�rer le nom du patient
	 * @return this.nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Permet de r�cup�rer la liste des hospitalisations du patient
	 * @return this.sejours
	 * @ArrayList
	 */
	public ArrayList<Hospitalisation> getSejours(){return sejours;}
	/**
	 * Permet d'ajouter une hospitalisation � la liste des s�hours du patient
	 * @param h une hospitalisation affili�e au patient
	 * @return un bool�en stipulant si l'ajout s'est effect� (true)
	 */
	public boolean addSejour(Hospitalisation h) {
		return sejours.add(h);
	}
	
	
	@Override
	public String toString() {
		String s = "PATIENT "+id+"\n"+dateNaissance+" _ "+(sexe==2?"F _":"H _")+prenom+" "+nom;
		s+="\n\tHospitalisation : "+sejours.size();
		//sejours.forEach((n)->s.concat("\n\t\t"+n));
		for(int i=0; i<sejours.size();i++) {
			s+="\n\t\t"+sejours.get(i);
		}
		return s;
	}
	
	@Override
	/**
	 * Redefinition de la m�thode equals, ne prenant en compte que l'identifiant unique du patient
	 */
	public boolean equals(Object obj) {
		if (this == obj)return true;
		if (getClass() != obj.getClass() || obj == null)return false;
		Patient p = (Patient) obj;
		if (id == null) {
			if (p.id != null)
				return false;
		} else if (!id.equals(p.id))
			return false;
		return true;
	}
	
	
	
	
}
