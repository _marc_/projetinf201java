/**
 * ProjetInf201, programme pour consulter des donn�es issues du PMSI
 */

/**
 * @author mafouque
 *
 */
package application.model;

import java.util.ArrayList;
import java.util.Scanner;
/**
 * Classe principale du projet, d�crivant le cheminement du programme
 * @author Marc
 *
 */
public class AppPMSI {

	/**
	 * le main
	 * @param args rien
	 */
	/*
	public static void main(String[] args) {
		System.out.println("AppPMSI");
		
		Scanner scanner = new Scanner( System.in );
		System.out.print("Entrez le nom de la base de donn�es : " );
		final String BD = scanner.nextLine();
		System.out.print("Entrez le num�ro de port MySQL : " );
		final String PORT = scanner.nextLine();
		
		
		
		
		//r�cup�ration des th�saurus
		Thesaurus[] thesaurus = {
				new Thesaurus("tab_ccam", "Classification Commune des Actes M�dicaux",BD,PORT),
				new Thesaurus("tab_cim10","Classification Internationale Medicale",BD,PORT)
				};
		 
		try {
			
			String stop = "O";
			//boucle pour arreter le programme
			while(stop.equals("O")) {
				String choix = "";
				//les trois possibilit�s
				String trois="3 : D�terminer les 5 diagnostics les plus fr�quents chez un sexe",deux="2 : D�terminer le nombre d�acte qu�un patient a subi au cours d�une p�riode",un = "1 : D�terminer les diff�rents s�jours d�un patient";
				
				do {//boucle pour confirmer le choix de l'action
					System.out.println( "Entrez le num�ro de l'action d�sir�e :" );
					System.out.print(un);
					System.out.println("\t\t\t"+deux);
					System.out.println(trois);
					System.out.print("num�ro : ");
					choix = scanner.nextLine();
				}while(!choix.equals("1") && !choix.equals("2") && !choix.equals("3"));
				
				String numPat = "updateDelete";

				switch (choix) {//les diff�rentes possibilit�s
				case "1":
					System.out.println(un);
					//boucle pour ne pas faire de b�tises (et encore...)
					while(numPat.toUpperCase().contains("UPDATE") || numPat.toUpperCase().contains("DELETE")) {
						System.out.print("Entrez l'identifiant du patient souhait� :" );
						numPat = scanner.nextLine();
					}
					int[] detailRetour = {0,3,1,0};//recuperation d'actes et de diagnostics imbriqu�s dans leur hospitalisation affili�e					ArrayList resul = new ArrayList();
					String requete = "SELECT * FROM `tab_acte`,tab_hospitalisation,tab_diagnostic where tab_acte.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION and tab_diagnostic.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION and tab_hospitalisation.ID_PATIENT="+numPat;
					System.out.println("requete lanc�e");
					ArrayList resul = UtilitaireRequete.requetePmsi(requete,detailRetour, thesaurus,BD,PORT);
					System.out.println("requete effectuee");
					resul.forEach((n)->System.out.println(n));
					break;
				case "2":
					System.out.println(deux);
					numPat="updateDelete";
					while(numPat.toUpperCase().contains("UPDATE") || numPat.toUpperCase().contains("DELETE")) {
						System.out.print("Entrez l'identifiant du patient souhait� :" );
						numPat = scanner.nextLine();
					}
					String date1="";
					String date2="";
					//boucles pour s'assurer des conformit�s des dates
					while(!date1.matches("[12][0-9]{3}-[01][0-9]-[0-3][0-9]")) {
						System.out.print("Entrez la date de d�but : (YYYY-MM-DD) " );
						date1 = scanner.nextLine();
					}
					while(!date2.matches("^[12][0-9]{3}-[01][0-9]-[0-3][0-9]$")) {
						System.out.print("Entrez la date de fin : (YYYY-MM-DD) " );
						date2 = scanner.nextLine();
					}
					int[] detailRetour2 = {0,0,0,0}; //trivial, retour num�rique
					ArrayList resul2 = new ArrayList();
					String requete2 = "select count(ID_AKT) from tab_acte,tab_hospitalisation where tab_hospitalisation.ID_PATIENT in (Select ID_PATIENT from tab_patient where ID_PATIENT="+numPat+") and tab_acte.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION and tab_acte.DATE_ATK>='"+date1+"' and tab_acte.DATE_ATK<='"+date2+"'  group by tab_hospitalisation.ID_PATIENT";
					System.out.println("requete lanc�e");
					resul2 = UtilitaireRequete.requetePmsi(requete2,detailRetour2, thesaurus,BD,PORT);
					System.out.println("requete effectuee");
					resul2.forEach((n)->System.out.println(n));
					
					break;
				case "3":
					System.out.println(trois);
					String sexe="updatedelete";
					//boucle pour s'assurer de la conformit� des sexes (1 ou 2)
					while((sexe.toUpperCase().contains("UPDATE") || sexe.toUpperCase().contains("DELETE")) && !sexe.contentEquals("1") && !sexe.equals("2")) {
						System.out.print("Entrez le sexe que vous voulez observer : (1:homme,2:femme) " );
						sexe = scanner.nextLine();
					}
					int[] detailRetour3 = {1,0,0,0};// retour d'une liste de classification
					ArrayList resul3 = new ArrayList();
					String requete3 = "Select CODE_CIM10 as code_classi from tab_diagnostic,tab_hospitalisation where tab_hospitalisation.ID_PATIENT in (Select ID_PATIENT from tab_patient where sexe="+sexe+") and tab_diagnostic.ID_HOSPITALISATION=tab_hospitalisation.ID_HOSPITALISATION group by CODE_CIM10 Order by count(ID_DISGNOCTIC) DESC LIMIT 5;";
					System.out.println("requete lanc�e");
					resul3 = UtilitaireRequete.requetePmsi(requete3,detailRetour3, thesaurus,BD,PORT);
					System.out.println("requete effectuee");
					resul3.forEach((n)->System.out.println(n));
					break;
					//default:
					//	break;
				}
				//boucle pour d�cider de l'arret ou non du programme
				do {
					System.out.print("Voulez-vous continuer? (O\\N) ");
					stop = scanner.nextLine();
				}while(!stop.toUpperCase().equals("O") && !stop.toUpperCase().equals("N"));
			}
		}catch(Exception e) {System.out.println(e+" "+e.getMessage());}
		

	}
*/
}
