package application.model;

import java.util.Date;
/**
 * Repr�sente un acte effectu� lors d'une hospitalisation
 * @see Operation
 */
public class Acte extends Operation {
	/**
	 * date_acte la date de l'acte
	 * anesth si oui ou non l'acte a �t� eff�ctu� sous anesth�sie
	 */
	private Date date_acte;
	private boolean anesth;
	
	/**
	 * Constructeur 
	 * @param ida l'identifiant de l'acte
	 * @param c la classification ccam de l'acte
	 * @see Classification
	 * @param d la date de l'acte
	 * @param b si l'acte a �t� �ff�ctu� sous anesth�sie
	 */
	public Acte(String ida,Classification c, Date d, boolean b) {
		super(ida,c);
		date_acte = d;
		anesth = b;
	}
	/**
	 * Constructeur
	 * @param ida
	 */
	public Acte(String ida) {
		super(ida);
	}
	/**
	 * Permet de r�cup�rer la date de l'acte
	 * @return this.date_acte , la date de l'acte
	 */
	public Date getDate_acte() {
		return date_acte;
	}
	/**
	 * Permet de recup�rer l'information d'anesthesie ou non
	 * @return this.anesth bool�en positif si l'acte a eu lieu sous anesthsie
	 */
	public boolean isAnesth() {
		return anesth;
	}
	
	
	@Override
	public String toString() {
		String s = "Acte "+this.getId()+(this.getDate_acte()!=null?" en date du "+this.getDate_acte():"")+(this.isAnesth()?" sous anesthesie ":" sans anesthesie")+"\n\t\t\tCCAM : "+this.getCode();
		return s; 
	}
}
