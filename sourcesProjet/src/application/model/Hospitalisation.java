package application.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
/**
 * Permet de repr�senter un s�jour � l'hopital avec un identifiant, les listes des actes et des diagnostics �ff�ctu�s lors du s�jours
 * et les date d'entr�e et sortie.
 * @see ArrayList
 * @see Date
 * @see Acte
 * @see Diagnostic
 * @author Marc
 *
 */
public class Hospitalisation {
	//proprietes
	private int id;
	private ArrayList <Acte> actes;
	private ArrayList <Diagnostik> diagnos;
	private Date dEntree;
	private Date dSortie;
	
	//constructeurs
	/**
	 * Constructeur
	 * @param i l'identifiant de l'hospitalisation
	 * @param a la liste des actes
	 * @param d la liste des diagnostics
	 * @param de la date d'entr�e � l'hopital
	 * @param ds la date de sortie de l'hopital
	 */
	public Hospitalisation(int i, ArrayList <Acte> a, ArrayList <Diagnostik> d, Date de, Date ds) {
		this.id = i;
		this.actes = new ArrayList<Acte>();
		this.diagnos = new ArrayList<Diagnostik>();
		this.dEntree = de;
		this.dSortie = ds;
	}
	/**
	 * Constructeur
	 * @param i l'identifiant unique de l'hospitalisation
	 * @param de la date d'entr�e � l'hopital
	 * @param ds la date de sortie de l'hopital
	 */
	public Hospitalisation(int i, Date de, Date ds) {
		this(i, null, null, de, ds);
	}
	
	/**
	 * Constructeur
	 * @param i l'identifiant unique de l'hospitalisation
	 * @param de la date d'entr�e � l'hopital
	 */
	public Hospitalisation(int i, Date de) {
		this(i, de, null);
	}
	
	/**
	 * Constructeur
	 * @param i l'identifiant unique de l'hospitalisation
	 */
	public Hospitalisation(int i) {
		this(i,null);
		//this.id = i;
	}
	
	//getters
	/**
	 * permet de r�cup�er l'identifiant de l'hospitalisation
	 * @return this.id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * permet de r�cup�rer la liste des actes �ff�ctu�s
	 * @return this.actes
	 * @see ArrayList
	 * @see Acte
	 */
	public ArrayList <Acte> getActes() {
		return actes;
	}
	/**
	 * Permet de r�cup�rer la liste des diagnostics �ffectu�s
	 * @return this.diagnos
	 * @see ArrayList
	 * @see Diagnostik
	 */
	public ArrayList <Diagnostik> getDiagnos() {
		return diagnos;
	}
	/**
	 * Permet de r�cup�rer la date d'entr�e � l'hopital
	 * @return this.dEntree
	 * @see Date
	 */
	public Date getdEntree() {
		return dEntree;
	}
	/**
	 * Permet de r�cup�rer la date de sortie de l'hopital
	 * @return dSortie
	 * @see Date
	 */
	public Date getdSortie() {
		return dSortie;
	}
	//setters
	/**
	 * Permet de remplacer la liste des actes
	 * @param a
	 * @see ArrayList
	 * @see Acte
	 */
	public void setActes(ArrayList<Acte> a) {
		this.actes = a;
	}
	/**
	 * Permet de remplacer la liste des diagnostics
	 * @param d
	 * @see ArrayList
	 * @see Diagnostik
	 */
	public void setDiagnos(ArrayList<Diagnostik> d) {
		this.diagnos = d;
	}
	
	//methodes
	/**
	 * Permet d'ajouter un acte ou un diagnostic � sa liste affili�e
	 * @param o un acte ou diagnostic
	 * @see Operation
	 * @see Acte
	 * @see Diagnostik
	 */
	public void addOperation(Operation o) {
		if(o instanceof Diagnostik)this.diagnos.add((Diagnostik)o);
		else if (o instanceof Acte)  this.actes.add((Acte)o);
	}
	
	
	@Override
	public String toString() {
		String s = "Hospitalisation "+this.getId();
		s+= (this.getdEntree()!=null?"\n\tDebut : "+this.getdEntree():"");
		s+= (this.getdSortie()!=null?"\n\tFin   : "+this.getdSortie():"");
		ArrayList <Diagnostik> dd = this.getDiagnos();
		ArrayList <Acte> aa = this.getActes();
		if(dd!=null) {
			s+="\n\tDiagnostic"+(dd.size()==1?"":"s");
			for (int i = 0; i < dd.size(); i++) {
				  s+="\n\t\t"+dd.get(i);
				}
		}
		if(dd!=null) {
			s+="\n\tActe"+(aa.size()==1?"":"s");
			for (int i = 0; i < aa.size(); i++) {
				  s+="\n\t\t"+aa.get(i);
				}
		}
		return s;
	}

	
	
	
	
}
