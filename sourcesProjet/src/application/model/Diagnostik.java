package application.model;

import java.util.Date;
/**
 * Permet de repr�senter un diagnostic
 * avec son rang et son type
 * @author Marc
 * @see Operation
 */
public class Diagnostik extends Operation {
	private int rang;
	private char type;
	
	//constructeurs
	/**
	 * Constructeur
	 * @param ida l'identifiant unique du diagnostic
	 * @param c la classification cim10 � laquelle il se rattache
	 * @param r son rang de 1 � 15
	 * @param t le type de diagnostic (R,S,T)
	 */
	public Diagnostik(String ida,Classification c, int r, char t) {
		super(ida,c);
		rang = r;
		type = t;
	}
	/**
	 * Constructeur
	 * @param ida l'identifiant unique du diagnostic
	 */
	public Diagnostik(String ida) {
		super(ida);
	}
	
	//getters
	/**
	 * permet de r�cup�rer le rang du diagnostic
	 * @return this.rang
	 */
	public int getRang() {
		return rang;
	}
	/**
	 * Permet de r�cup�rer le type du diagnostic
	 * @return this.type
	 */
	public char getType() {
		return type;
	}
	
	@Override
	public String toString() {
		String s = "Diagnostic "+super.getId()+(this.getRang()>0 && this.getRang()<16?" de rang "+this.getRang():"")+(this.getType()=='S' || this.getType()=='D' || this.getType()=='S'?" de niveau "+this.getType():"")+"\n\t\t\tCIM10 : "+super.getCode();
		return s; 
	}
	
	
	
}
